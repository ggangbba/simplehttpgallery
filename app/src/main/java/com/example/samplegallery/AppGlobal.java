package com.example.samplegallery;

import android.graphics.Point;

import com.example.samplegallery.utils.ScreenUtil;
import com.example.samplegallery.utils.network.HttpManager;

public class AppGlobal {
    // use as singleton
    private static AppGlobal instance;
    private HttpManager httpManager;
    private AppGlobal() {
        httpManager = HttpManager.getInstance();
    }

    public static AppGlobal getInstance() {
        if (instance == null) {
            instance = new AppGlobal();
        }
        return instance;
    }

    public HttpManager getHttpManager() {
        return httpManager;
    }


    /*
        For adjust the request of bitmap size
     */
    private int targetWidth = ScreenUtil.INVALID_SCREEN_SIZE;
    private int targetHeight = ScreenUtil.INVALID_SCREEN_SIZE;

    public int getTargetWidth() {
        return targetWidth;
    }

    public void setTargetWidth(int targetWidth) {
        this.targetWidth = targetWidth;
    }

    public int getTargetHeight() {
        return targetHeight;
    }

    public void setTargetHeight(int targetHeight) {
        this.targetHeight = targetHeight;
    }
}
