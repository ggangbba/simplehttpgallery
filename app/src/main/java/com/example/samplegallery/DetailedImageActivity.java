package com.example.samplegallery;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.samplegallery.ui.ZoomableLinearLayout;
import com.example.samplegallery.utils.KYLog;
import com.example.samplegallery.utils.imageloader.SashaGlide;

public class DetailedImageActivity extends AppCompatActivity{

    private ImageView mDetailImageView;
    private ProgressBar mProgressIndicator;
    private ZoomableLinearLayout mZoomLinearLayout;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        KYLog.D(">>>>");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_image);

        mDetailImageView = findViewById(R.id.detail_image_view);
        mProgressIndicator = findViewById(R.id.progress_bar);

        // just for user convenience, can remove.
        mZoomLinearLayout = findViewById(R.id.zoom_linear_layout);
        mZoomLinearLayout.setOnTouchListener((v, event) -> {
            mZoomLinearLayout.init(DetailedImageActivity.this);
            return false;
        });

        String uriPath = getIntent().getStringExtra("URI");
        SashaGlide.with(mDetailImageView)
                .load(uriPath)
                .listener(createLoadingListener())
                .into(mDetailImageView);
        KYLog.D("<<<<");
    }

    @Override
    protected void onStop() {
        KYLog.D(">>>>");
        super.onStop();
        KYLog.D("<<<<");
    }

    private RequestListener<Drawable> createLoadingListener() {
        return new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (resource instanceof BitmapDrawable) {
                    Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                    KYLog.d("DBG-Glide"
                            , "Ready " + "Origin" + " Bitmap "
                                    + "[" + bitmap.getByteCount() + "] bytes,"
                                    + "Size " + bitmap.getWidth() + " x " + bitmap.getHeight());

                    // stop indicator
                    if (mProgressIndicator.getVisibility() == View.VISIBLE) {
                        mProgressIndicator.setVisibility(View.GONE);
                    }

                    // apply size value to layout
                    mZoomLinearLayout.getLayoutParams().width = bitmap.getWidth();
                    mZoomLinearLayout.getLayoutParams().height = bitmap.getHeight();
                    mZoomLinearLayout.invalidate();
                }
                return false;
            }
        };
    }
}
