package com.example.samplegallery.model;

public final class Image {
    private String title;
    private String path;

    @Override
    public String toString() {
        return "Image{"
                + ", title='"
                + title
                + '\''
                + ", path='"
                + path
                + '\''
                + '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
