package com.example.samplegallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.samplegallery.adapter.ImageAdapter;
import com.example.samplegallery.model.Image;
import com.example.samplegallery.utils.KYLog;
import com.example.samplegallery.utils.network.HttpManager;
import com.example.samplegallery.utils.network.HttpResponseListener;

import java.util.List;

public class MainActivity extends AppCompatActivity implements HttpResponseListener {

    private AppGlobal       mGlobal;
    private HttpManager     mHttpManager;
    private ImageAdapter    mImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        KYLog.D(">>>>");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // init views
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mImageAdapter = new ImageAdapter(this);
        recyclerView.setAdapter(mImageAdapter);

        // init app instance
        mGlobal = AppGlobal.getInstance();
        /*
         * In this sample, determines bitmap size to 512x512 to decrease loading time.
         * (In a user service, if screen size is smaller than bitmap size,
         *  adjust request width and height of Glide as decreasing its size.)
         * */
        final int LIST_ITEM_BITMAP_SIZE = 512;
        mGlobal.setTargetWidth(LIST_ITEM_BITMAP_SIZE);
        mGlobal.setTargetHeight(LIST_ITEM_BITMAP_SIZE);

        mHttpManager = mGlobal.getHttpManager();
        mHttpManager.addCallback(this);
        mHttpManager.execute();
        KYLog.D("<<<<");
    }

    @Override
    protected void onStop() {
        KYLog.D(">>>>");
        super.onStop();
        mHttpManager.removeCallback(this);
        KYLog.D("<<<<");
    }

    @Override
    public void onReceivedResponse(int status, List<Image> imageList) {
        KYLog.D(">>>>");
        // Must be processed in UI thread
        runOnUiThread(() -> {
            if (imageList != null) {
                mImageAdapter.setData(imageList);
                mImageAdapter.notifyDataSetChanged();
                dismissProgressBar();
            }
        });
        KYLog.D("<<<<");
    }

    @Override
    public void onReceivedFailure(int status) {
        KYLog.D(">>>>");
        // Must be processed in UI thread
        runOnUiThread(() -> {
            Toast.makeText(this, R.string.msg_err_http_rp, Toast.LENGTH_SHORT).show();
            dismissProgressBar();
        });
        KYLog.D("<<<<");
    }

    private void dismissProgressBar() {
        ProgressBar progressBar = findViewById(R.id.progress_bar);
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }
}
