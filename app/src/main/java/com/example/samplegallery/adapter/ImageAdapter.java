package com.example.samplegallery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.samplegallery.AppGlobal;
import com.example.samplegallery.DetailedImageActivity;
import com.example.samplegallery.R;
import com.example.samplegallery.utils.KYLog;
import com.example.samplegallery.utils.ScreenUtil;
import com.example.samplegallery.utils.imageloader.SashaGlide;
import com.example.samplegallery.model.Image;

import java.util.Collections;
import java.util.List;

public final class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private Context context;
    private List<Image> images = Collections.emptyList();

    public ImageAdapter(Context context) {
        this.context = context;
    }

    public void setData(@NonNull List<Image> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageAdapter.ViewHolder holder, int position) {
        Image image = images.get(position);
        if (!image.getTitle().equals("")) {
            holder.textView.setText(image.getTitle());
        }

        AppGlobal global = AppGlobal.getInstance();
        int targetWidth = global.getTargetWidth();
        int targetHeight = global.getTargetHeight();
        if (targetWidth != ScreenUtil.INVALID_SCREEN_SIZE
                && targetHeight != ScreenUtil.INVALID_SCREEN_SIZE) {
            SashaGlide.with(holder.imageView)
                    .load(image.getPath())
                    .override(targetWidth, targetHeight)
                    .listener(createOnLoggingListener())
                    .into(holder.imageView);
        } else {
            SashaGlide.with(holder.imageView)
                    .load(image.getPath())
                    .listener(createOnLoggingListener())
                    .into(holder.imageView);
        }
    }

    // for debug info of Glide request
    // refer to https://soulduse.tistory.com/72
    private RequestListener<Drawable> createOnLoggingListener() {
        return new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (resource instanceof BitmapDrawable) {
                    Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                    KYLog.d("DBG-Glide"
                            , "Ready " + "Origin" + " Bitmap "
                                    + "[" + bitmap.getByteCount() + "] bytes,"
                                    + "Size " + bitmap.getWidth() + " x " + bitmap.getHeight());
                }
                return false;
            }
        };
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView textView;
        private final ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.item_image_view);
            textView = itemView.findViewById(R.id.item_text_view);

            // set onCLick listener in constructor
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, DetailedImageActivity.class);
            intent.putExtra("URI", images.get(getAdapterPosition()).getPath());
            context.startActivity(intent);
        }
    }
}