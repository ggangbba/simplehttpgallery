package com.example.samplegallery.utils.network;

import com.example.samplegallery.model.Image;

import java.util.List;

public interface HttpResponseListener {
    void onReceivedResponse(int status, List<Image> imageList);
    void onReceivedFailure(int status);
}
