package com.example.samplegallery.utils;

import android.util.Log;

public class KYLog {

    public enum LogLevel {
        ERROR(0),
        WARNING(1),
        DEBUG(2),
        INFO(3),
        VERBOSE(4);

        private int value;

        LogLevel(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private static LogLevel mLevel = LogLevel.DEBUG;

    public KYLog() {
    }

    public static void setLogLevel(LogLevel level) {
        mLevel = level;
    }

    private static String buildLogTag() {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append(ste.getFileName());
        sb.append(" > ");
        sb.append(ste.getMethodName());
        sb.append(" > #");
        sb.append(ste.getLineNumber());
        sb.append("] ");

        return sb.toString();
    }

    private static String buildLogMsg(String message) {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append(ste.getFileName());
        sb.append(" > ");
        sb.append(ste.getMethodName());
        sb.append(" > #");
        sb.append(ste.getLineNumber());
        sb.append("] ");
        sb.append(message);

        return sb.toString();
    }

    public static void e(String tag, String message) {
        if (mLevel.getValue() >= LogLevel.ERROR.getValue()) {
            String log = buildLogMsg(message);
            Log.e(tag, log);
        }
    }

    public static void w(String tag, String message) {
        if (mLevel.getValue() >= LogLevel.WARNING.getValue()) {
            String log = buildLogMsg(message);
            Log.w(tag, log);
        }
    }

    public static void i(String tag, String message) {
        if (mLevel.getValue() >= LogLevel.INFO.getValue()) {
            String log = buildLogMsg(message);
            Log.i(tag, log);
        }
    }

    public static void d(String tag, String message) {
        if (mLevel.getValue() >= LogLevel.DEBUG.getValue()) {
            String log = buildLogMsg(message);
            Log.d(tag, log);
        }
    }

    public static void v(String tag, String message) {
        if (mLevel.getValue() >= LogLevel.VERBOSE.getValue()) {
            String log = buildLogMsg(message);
            Log.v(tag, log);
        }
    }


    public static void E(String msg) {
        e(buildLogTag(), buildLogMsg(msg));
    }

    public static void W(String msg) {
        w(buildLogTag(), buildLogMsg(msg));
    }

    public static void D(String msg) {
        d(buildLogTag(), buildLogMsg(msg));
    }

    public static void I(String msg) {
        i(buildLogTag(), buildLogMsg(msg));
    }

    public static void V(String msg) {
        v(buildLogTag(), buildLogMsg(msg));
    }
}
