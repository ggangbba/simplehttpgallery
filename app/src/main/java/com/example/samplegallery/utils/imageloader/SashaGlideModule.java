package com.example.samplegallery.utils.imageloader;

import android.content.Context;

import androidx.annotation.NonNull;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

// In glide 4.x version, use particular module as below
// Refer to https://bumptech.github.io/glide/doc/configuration.html#application-options
@GlideModule(glideName = "SashaGlide")
public class SashaGlideModule extends AppGlideModule {
    @Override
    public void applyOptions(@NonNull Context context, GlideBuilder builder) {

        /*
         * Note that MemorySizeCalculator calculates cache size based on screen size and density of the device.
         * LruResourceCache is implementation of MemoryCache. And it uses the size of calculated by MemorySizeCalculator to cache .
         *  */

        // 1. determines the memory cache size
        // 2. determines the BitmapPool size that can be done similar with memory cache strategy
        MemorySizeCalculator calculator = new MemorySizeCalculator.Builder(context)
                .setMemoryCacheScreens(1)
                .setBitmapPoolScreens(1)
                .build();
        // also can be applied as hard coded
        //int memoryCacheSizeBytes = 1024 * 1024 * 20; // 20mb
        builder.setMemoryCache(new LruResourceCache(calculator.getMemoryCacheSize()));
        builder.setBitmapPool(new LruBitmapPool(calculator.getBitmapPoolSize()));

        // 3. determines the disk cache size
        int diskCacheSizeBytes = 1024 * 1024 * 100; // 100 MB, heuristically value
        builder.setDiskCache(new InternalCacheDiskCacheFactory(context, diskCacheSizeBytes));

        // 4. determines default options for decoding format of resource and caching targets
        builder.setDefaultRequestOptions(
                new RequestOptions()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE));
    }
}