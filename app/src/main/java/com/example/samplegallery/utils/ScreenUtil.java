package com.example.samplegallery.utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import androidx.annotation.NonNull;

public class ScreenUtil {

    public static final int INVALID_SCREEN_SIZE = -9999;

    /*
    * Don't use methods in this sample.
    * */

    public static int getScreenWidth(@NonNull Context context) {
        KYLog.D(">>>>");
        int width = INVALID_SCREEN_SIZE;
        WindowManager windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            Display display = windowManager.getDefaultDisplay();
            Point screenResolution = new Point();
            display.getRealSize(screenResolution);
            width = screenResolution.x;
        }
        KYLog.D("<<<<");
        return width;
    }

    public static int getScreenHeight(@NonNull Context context) {
        KYLog.D(">>>>");
        int height = INVALID_SCREEN_SIZE;
        WindowManager windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            Display display = windowManager.getDefaultDisplay();
            Point screenResolution = new Point();
            display.getRealSize(screenResolution);
            height = screenResolution.y;
        }
        KYLog.D("<<<<");
        return height;
    }

    public static int getResizedDisplayHeight(@NonNull Context context, int displayWidth) {
        KYLog.D(">>>>");
        int width = getScreenWidth(context);
        int height = getScreenHeight(context);
        int displayHeight = INVALID_SCREEN_SIZE;

        // poly = screen width : display width = screen height : display height
        if (width != INVALID_SCREEN_SIZE
                && height != INVALID_SCREEN_SIZE
                && displayWidth > 0) {
            displayHeight = (displayWidth * height) / width;
        }
        KYLog.D("<<<<");
        return displayHeight;
    }
}
