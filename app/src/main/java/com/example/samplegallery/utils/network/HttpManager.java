package com.example.samplegallery.utils.network;

import androidx.annotation.NonNull;

import com.example.samplegallery.model.Image;
import com.example.samplegallery.utils.KYLog;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* Referred to JSoup sample : https://blogdeveloperspot.blogspot.com/2017/11/jsoup-with-glide.html */

public class HttpManager {
    private static HttpManager instance;

    public static final String BASE_URL = "https://www.gettyimagesgallery.com/collection/sasha/";
    public static final String DETAIL_URL = "DETAIL_URL";
    public static final int STATE_OK = 200;


    private ArrayList<HttpResponseListener> callbacks = new ArrayList<>();

    private HttpManager() {}
    public static HttpManager getInstance() {
        if (instance == null) {
            instance = new HttpManager();
        }
        return instance;
    }

    private void onReceived(int status, List<Image> result) {
        KYLog.D(">>>>");
        for(HttpResponseListener listener : callbacks) {
            listener.onReceivedResponse(status, result);
        }
        KYLog.D("<<<<");
    }

    private void onFailure(int status) {
        KYLog.D(">>>>");
        for(HttpResponseListener listener : callbacks) {
            listener.onReceivedFailure(status);
        }
        KYLog.D("<<<<");
    }

    public void execute() {
        KYLog.D(">>>>");
        new Thread(() -> {
            ArrayList<Image> imageList = new ArrayList<>();

            try {
                Connection.Response response = Jsoup.connect(BASE_URL).execute();
                int status = response.statusCode();
                if (status != STATE_OK) {
                    onFailure(status);
                } else {
                    Document doc = response.parse();
                    Elements imageElems = doc.select(".item-wrapper");

                    for (Element elem : imageElems) {
                        Image item = new Image();
                        item.setTitle(elem.select("h5.image-title").text());
                        item.setPath(elem.select("img").attr("data-src"));
                        // for debugging
                        KYLog.D("Item Info >> \n" + item.toString() + "\n <<");
                        imageList.add(item);
                    }
                    onReceived(response.statusCode(), imageList);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        KYLog.D("<<<<");
    }

    public void addCallback(@NonNull HttpResponseListener listener) {
        callbacks.add(listener);
    }

    public void removeCallback(@NonNull HttpResponseListener listener) {
        callbacks.remove(listener);
    }
}
